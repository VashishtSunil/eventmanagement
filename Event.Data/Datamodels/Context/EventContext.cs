using EntityFramework.DynamicFilters;
using Event.Core;
using Event.Core.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;

namespace Event.Data.Datamodels.Context
{
    public class EventContext : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<EventContext>(null);
            base.OnModelCreating(modelBuilder);

            // for soft delete filter 
            modelBuilder.Filter("IsDeleted",
             (ISoftDelete d) => d.IsDeleted, false);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

        }
        public EventContext()
            : base("EventConnection")
        {
        }

        #region DbSets     
        public DbSet<Runner> Runners { get; set; }
        //public DbSet<ErrorMessage> ErrorMessages { get; set; }
        //public DbSet<AttachmentMapping> AttachmentMappings { get; set; }
        //public DbSet<Activity> Activities { get; set; }
        //public DbSet<Language> Languages { get; set; }
        //public DbSet<RequestResponse> RequestResponses { get; set; }
        //public DbSet<Venue> Venues { get; set; }
        //public DbSet<Offer> Offers { get; set; }
        //public DbSet<Active> ActiveOffers { get; set; }
        //public DbSet<Reseller> Resellers { get; set; }
        //public DbSet<Brand> Brands { get; set; }
        //public DbSet<ResellerMember> ResellerMembers { get; set; }
        //public DbSet<ProgramStyle> ProgramStyles { get; set; }
        //public DbSet<Program> Programs { get; set; }
        //public DbSet<ProgramPunchCard> ProgramPunchCards { get; set; }
        //public DbSet<ProgramPoint> ProgramPoints { get; set; }
        //public DbSet<LookUpDomain> LookUpDomains { get; set; }
        //public DbSet<LookUpDomainValue> LookUpDomainValues { get; set; }
        //public DbSet<ProgramPointTerm> ProgramPointTerms { get; set; }
        //public DbSet<ApiUser> ApiUsers { get; set; }
        //public DbSet<ApiToken> ApiTokens { get; set; }
        ////programRedemption
        //public DbSet<BarcodeStyle> BarcodeStyles { get; set; }
        //public DbSet<RedemptionStyle> RedemptionStyles { get; set; }
        //public DbSet<ProgramRedemption> ProgramRedemptions { get; set; }
        //public DbSet<RedemptionVoucher> RedemptionVouchers { get; set; }
        //public DbSet<RedemptionCoupon> RedemptionCoupons { get; set; }

        ////Retailer
        //public DbSet<Retailer> Retailers { get; set; }

        ////Members 
        //public DbSet<Member> Members { get; set; }
        //public DbSet<MemberRedemptionVoucher> MemberRedemptionVouchers { get; set; }
        //public DbSet<Transaction> Transactions { get; set; }
        //public DbSet<MemberPoint> MemberPoints { get; set; }
        //public DbSet<Location> Locations { get; set; }

        ////Location
        //public DbSet<Country> Countries { get; set; }
        //public DbSet<State> States { get; set; }
        //public DbSet<Region> Regions { get; set; }

        ////Region
        //public DbSet<RegionCountry> RegionCountries { get; set; }
        //public DbSet<RegionState> RegionStates { get; set; }
        //public DbSet<RegionMaster> RegionMasters { get; set; }
        #endregion



        // Automatically add the times the entity got created/modified
        public override int SaveChanges()
        {
            string tempInfo = String.Empty;

            var entries = ChangeTracker.Entries().ToList();
            for (int i = 0; i < entries.Count; i++)
            {
                if (entries[i].State == EntityState.Unchanged || entries[i].State == EntityState.Detached || entries[i].State == EntityState.Deleted) continue;

                var hasInterfaceInheritDb = entries[i].Entity as BaseEntity;
                if (hasInterfaceInheritDb == null) continue;

                if (entries[i].State == EntityState.Added)
                {
                    var created = entries[i].Property("CreatedOn");
                    if (created != null)
                    {
                        created.CurrentValue = DateTime.UtcNow;
                    }
                }
                if (entries[i].State == EntityState.Modified)
                {
                    var modified = entries[i].Property("ModifiedOn");
                    if (modified != null)
                    {
                        modified.CurrentValue = DateTime.UtcNow;
                    }
                }
            }
            return base.SaveChanges();
        }

        #region Methods/Functions
        protected virtual TEntity AttachEntityToContext<TEntity>(TEntity entity) where TEntity : BaseEntity, new()
        {
            //little hack here until Entity Framework really supports stored procedures
            //otherwise, navigation properties of loaded entities are not loaded until an entity is attached to the context
            var alreadyAttached = Set<TEntity>().Local.FirstOrDefault(x => x.Id == entity.Id);
            if (alreadyAttached == null)
            {
                //attach new entity
                Set<TEntity>().Attach(entity);
                return entity;
            }
            else
            {
                //entity is already loaded.
                return alreadyAttached;
            }
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : Core.BaseEntity
        {
            return base.Set<TEntity>();
        }

        public IList<TEntity> ExecuteStoredProcedureList<TEntity>(string commandText, params object[] parameters) where TEntity : Core.BaseEntity, new()
        {
            //add parameters to command
            if (parameters != null && parameters.Length > 0)
            {
                for (int i = 0; i <= parameters.Length - 1; i++)
                {
                    var p = parameters[i] as DbParameter;
                    if (p == null)
                        throw new Exception("Not support parameter type");

                    commandText += i == 0 ? " " : ", ";

                    commandText += "@" + p.ParameterName;
                    if (p.Direction == ParameterDirection.InputOutput || p.Direction == ParameterDirection.Output)
                    {
                        //output parameter
                        commandText += " output";
                    }
                }
            }

            var result = this.Database.SqlQuery<TEntity>(commandText, parameters).ToList();
            bool acd = this.Configuration.AutoDetectChangesEnabled;
            try
            {
                this.Configuration.AutoDetectChangesEnabled = false;

                for (int i = 0; i < result.Count; i++)
                    result[i] = AttachEntityToContext(result[i]);
            }
            finally
            {
                this.Configuration.AutoDetectChangesEnabled = acd;
            }

            return result;
        }

        public IEnumerable<TElement> SqlQuery<TElement>(string sql, params object[] parameters)
        {
            return this.Database.SqlQuery<TElement>(sql, parameters);
        }

        public int ExecuteSqlCommand(string sql, bool doNotEnsureTransaction = false, int? timeout = null, params object[] parameters)
        {
            int? previousTimeout = null;
            if (timeout.HasValue)
            {
                //store previous timeout
                previousTimeout = ((IObjectContextAdapter)this).ObjectContext.CommandTimeout;
                ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = timeout;
            }

            var transactionalBehavior = doNotEnsureTransaction
                ? TransactionalBehavior.DoNotEnsureTransaction
                : TransactionalBehavior.EnsureTransaction;
            var result = this.Database.ExecuteSqlCommand(transactionalBehavior, sql, parameters);

            if (timeout.HasValue)
            {
                //Set previous timeout back
                ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = previousTimeout;
            }

            //return result
            return result;
        }

        #endregion

    }

}
