﻿using CouponFactory.Core.Models;
using Event.Core.Models;
using Event.Services.Classes.Runner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Event.UI.Controllers
{
    [Authorize]
    public class AdminRunnerController : Controller
    {
        // GET: AdminRunner
        public ActionResult Index()
        {
            return View();
        }



        /// <summary>
        /// Get Brands List
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        public async Task<ActionResult> List(jQueryDataTableParamModel parms)
        {
            var runners = await RunnerManager.GetRunnersDataTable(parms);
            return Json(new
            {
                aaData = runners.Item1,
                iTotalRecords = runners.Item3,
                iTotalDisplayRecords = runners.Item3
            }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Open Runner Modal
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OpenRunnerModal(long id)
        {
            if (!Request.IsAjaxRequest())
                return null;

            RunnerModel runnerModel = new RunnerModel();
            if (id > 0)
            {
                runnerModel = RunnerManager.GetRunnerById(id);
            }
            
            return PartialView("~/Views/AdminRunner/_AddEdit.cshtml", runnerModel);
        }   



        /// <summary>
        /// Add Runner
        /// </summary>
        /// <param name="runnerModel"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddRunner(RunnerModel runnerModel)
        {
            ModelState.Remove("Id");
            string successMessage = (runnerModel.Id != 0) ? "Runner Updated Successfully!" : " Runner Added Successfully!";
            string errMsg = "Something Went Wrong!";
            try
            {
                if (ModelState.IsValid)
                {
                    var result = RunnerManager.UpdateRunner(runnerModel); ;
                    if (result)
                    {
                        return Json(new
                        {
                            success = true,
                            message = successMessage,
                            JsonRequestBehavior.AllowGet
                        });
                    }
                }
                return Json(new
                {
                    success = false,
                    message = errMsg,
                    JsonRequestBehavior.AllowGet
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = errMsg,
                    JsonRequestBehavior.AllowGet
                });
            }
        }


        public JsonResult DeleteRunner(long Id)
        {
            try
            {
                bool result = RunnerManager.DeleteRunner(Id);
                return Json(new
                        {
                            success = (result) ? true : false,
                            message = (result) ? "Runner Deleted" : "Error Occurred While Deleting",
                            JsonRequestBehavior.AllowGet
                        });;
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = true,
                    message = "Error Occurred While Deleting",
                    JsonRequestBehavior.AllowGet
                }); ;
            }
        }

    }
}