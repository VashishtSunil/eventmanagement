﻿using CouponFactory.Core.Models;
using Event.Core.Models;
using Event.Services.Classes.Runner;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Event.UI.Controllers
{
    public class RunnerController : Controller
    {
        // GET: Runner
        public ActionResult Index()
        {
            RunnerModel runnerModel = new RunnerModel();

            //var citiesList = new SelectList(
            //    new List<SelectListItem>
            //    {
            //        new SelectListItem {Text = "Bath", Value = "Bath"},
            //        new SelectListItem {Text = "Birmingham ", Value = "Birmingham "},
            //         new SelectListItem {Text = "Bangor ", Value = "Bangor "},
            //          new SelectListItem {Text = "Newport ", Value = "Newport "},
            //          new SelectListItem {Text = "Aberdeen ", Value = "Aberdeen "},
            //          new SelectListItem {Text = "Lisburn  ", Value = "Lisburn  "},
            //    }, "Value", "Text");

            return View(runnerModel);
        }

        /// <summary>
        /// Register new Runner
        /// </summary>
        /// <param name="runnerModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Register(RunnerModel runnerModel)
        {

            ViewBag.Message = "Error occurred. Please try again after sometime.";
            try
            {
                if (ModelState.IsValid)
                {
                    bool result = RunnerManager.UpdateRunner(runnerModel);
                    ViewBag.Message = (result) ? "Thanks for registration" : "Error occurred. Please try again after sometime.";
                    return View("~/Views/Runner/AfterRegistartion.cshtml");
                }
                return View("~/Views/Runner/AfterRegistartion.cshtml");
            }
            catch (Exception ex)
            {

            }
            return View("~/Views/Runner/AfterRegistartion.cshtml");
        }


        /// <summary>
        /// Get Brands List
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        public async Task<ActionResult> List(jQueryDataTableParamModel parms)
        {
            var programs = await RunnerManager.GetRunnersDataTable(parms);
            return Json(new
            {
                aaData = programs.Item1,
                iTotalRecords = programs.Item3,
                iTotalDisplayRecords = programs.Item3
            }, JsonRequestBehavior.AllowGet);

        }

    }
}