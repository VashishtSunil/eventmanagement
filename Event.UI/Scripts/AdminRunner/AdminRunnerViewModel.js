﻿var AdminRunnerViewModel = new function () {
    var thisViewModel = this;
    //#region Runner

    var tblAdminRunner = "tblAdminRunner";
    this.runnerListView = function () {
        var responsiveDt = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        $('#' + tblAdminRunner).dataTable({
            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            cache: true,
            "responsive": true,
            "bServerSide": true,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource":"/adminrunner/list",
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveDt) {
                    responsiveDt = new ResponsiveDatatablesHelper($('#' + tblAdminRunner), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {
                responsiveDt.createExpandIcon(nRow);
            },
            "drawCallback": function () {
                responsiveDt.respond();
                $("a.edit").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Edit Runner'
                });
                $("a.edit").removeAttr("title");
                $("a.delete").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Delete Runner'
                });
                $("a.delete").removeAttr("title");
            },
            "aoColumns": [

                { "mDataProp": "Name", "sClass": "center" },
                { "mDataProp": "Address", "sClass": "center" },
                { "mDataProp": "HostingCity", "sClass": "center" },
                { "mDataProp": "Route", "sClass": "center" },
                { "mDataProp": "Charity", "sClass": "center" },
                { "mDataProp": "CharityFund", "sClass": "center" },
                { "mDataProp": "Category", "sClass": "center" },
                {
                    "orderable": false,
                    "mRender": function (data, type, full) {
                        return "<a  class='edit' onClick=AdminRunnerViewModel.updateRunner('" + full.Id + "')><i class='far fa-edit text-primary'></i></a> " +
                            "<a class='delete'  data-runnerId=" + full.Id + "  data-name=" + full.Name + " onClick=AdminRunnerViewModel.deleteRunner(this)><i class='fas fa-trash-alt text-danger'></i></a>";
                    }
                }
            ]
            ,
            "autoWidth": true
        });
    };


    // open update runner modal with data
    this.updateRunner = function (runnerId) {
        this.openRunnerModal(runnerId);

    };
    //open Brand model
    this.openRunnerModal = function (runnerId) {
        GetData("/adminrunner/openrunnermodal", openModalSuccess, {
            formId: "frmAddNewRunner",
            id: runnerId
        });
    };
      // open  runner success method
    this.updateRunnerSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            closeCommonModel();
            refreshDataTable(tblAdminRunner);
        }
    };
    //delete program success
    this.deleteRunnerSuccess = function (result) {
        if (result) {
            SwalSuccess("Record deleted successfully!");
            refreshDataTable(tblAdminRunner);
        }
        else {
            SwalError("Something went wrong!");
            refreshDataTable(tblAdminRunner);
        }
    };

    // delete Runner
    this.deleteRunner = function (e) {
        var data = $(e).data();
        swal({
            title: "Are you sure?",
            text: "Please confirm you wish to delete Runner " + data.name,
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
            .then((willDelete) => {
                if (willDelete) {
                    PostData("/adminrunner/deleterunner", this.deleteRunnerSuccess, { id: data.runnerid });

                }
            });
    };

    //#endregion User


};