﻿using System.Web;
using System.Web.Optimization;

namespace Event.UI
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Jquery 
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Content/lib/jquery/jquery.js",
                        "~/Content/lib/jquery.blockUI.js"
                        ));
            // Jquery Validte 
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Content/lib/jquery-validation/dist/jquery.validate.min.js"));
            //Site Js 
            bundles.Add(new ScriptBundle("~/bundles/masteradmin").Include(
                    "~/Content/lib/bootstrap/dist/js/bootstrap.bundle.js",
                      "~/Content/lib/jquery-ui/jquery-ui.js",
                      "~/Content/js/adminlte.min.js",
                     "~/Content/lib/datatables/jquery.dataTables.min.js",
                     "~/Content/lib/datatables-bs4/js/dataTables.bootstrap4.min.js",
                     "~/Content/js/datatables.responsive.js",
                     "~/Content/lib/summernote/summernote-bs4.min.js",
                     "~/Content/lib/ekko-lightbox/ekko-lightbox.min.js",
                     "~/Content/lib/jquery-validation/dist/jquery.validate.min.js",
                     "~/Content/lib/jquery-validation-unobtrusive/jquery.validate.unobtrusive.min.js",
                     "~/Content/lib/toastr/toastr.min.js",
                     "~/Content/lib/ladda/spin.min.js",
                     "~/Content/lib/ladda/ladda.min.js",
                     "~/Content/lib/ladda/ladda.jquery.min.js",
                     "~/Content/lib/sweetalert/sweetalert.min.js",
                     "~/Content/lib/select2/js/select2.full.min.js",
                     "~/Content/lib/datepicker/js/bootstrap-datepicker.min.js",
                     "~/Content/lib/moment/moment.min.js",
                     "~/Content/lib/daterangepicker/daterangepicker.js",
                     "~/Content/lib/bootstrap-confirm-button/bootstrap-confirm-button.min.js",
                     "~/Content/lib/dropzone/dist/dropzone.js",
                     "~/Content/dist/js/adminlte.min.js",
                     "~/Content/js/Constants.js",
                     "~/Content/js/Utils.js",

                     "~/Content/js/MainViewModel.js",
                     "~/Content/js/site.js",
                     "~/Content/js/custom.js"
                  ));
            //Css 
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/lib/fontawesome-free/css/all.min.css",
                      "~/Content/lib/font-awesome/css/all.min.css",
                      "~/Content/lib/icheck-bootstrap/icheck-bootstrap.min.css",
                      "~/Content/css/adminlte.min.css",
                      "~/Content/css/site.css",
                      "~/Content/css/ionicons.min.css",
                      "~/Content/lib/datatables-bs4/css/dataTables.bootstrap4.min.css",
                      "~/Content/lib/datepicker/css/bootstrap-datepicker.css",
                      "~/Content/lib/toastr/toastr.css",
                      "~/Content/lib/ladda/ladda-themeless.min.css",
                      "~/Content/lib/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css",
                      "~/Content/lib/select2/css/select2.min.css",
                      "~/Content/lib/daterangepicker/daterangepicker.css",
                      "~/Content/lib/bootstrap4-duallistbox/bootstrap-duallistbox.min.css",
                      "~/Content/lib/summernote/summernote-bs4.css",
                      "~/Content/lib/ekko-lightbox/ekko-lightbox.css",
                      "~/Content/lib/dropzone/dist/dropzone.css",
                       "~/Content/css/custom.min.css"
                      ));
        }
    }
}
