﻿var UserManagerViewModel = new function () {
    var thisViewModel = this;
    //#region User
    //for user
    var tblRole = "tblRole";
    var tblUser = "tblUser";
    this.userListView = function () {
        var responsiveDt = undefined;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        $('#' + tblUser).dataTable({
            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            cache: true,
            responsive: true,
            "bServerSide": false,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource": "/admin/usermanager/getusers/",
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveDt) {
                    responsiveDt = new ResponsiveDatatablesHelper($('#tblUser'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, data) {
                responsiveDt.createExpandIcon(nRow);
            },
            "drawCallback": function () {
                responsiveDt.respond();
                $("a.edit").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Edit User'
                });
                $("a.edit").removeAttr("title");
                $("a.delete").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Delete User'
                });
                $("a.delete").removeAttr("title");
            },
            "aoColumns": [
                { "mDataProp": "userName", "sClass": "center" },
                { "mDataProp": "email", "sClass": "center" },
                {
                    "orderable": false,
                    "mRender": function (data, type, full) {
                        return "<a  class='edit' onClick=UserManagerViewModel.updateUser('" + full.id + "')><i class='far fa-edit text-primary'></i></a> " +
                            "<a class='delete' onClick=UserManagerViewModel.deleteUser('" + full.id + "')><i class='fas fa-trash-alt text-danger'></i></a>";
                    }
                }
            ],
            "autoWidth": true
        });
    };
    // open  update User modal with data
    this.updateUser = function (userId) {
        this.openUserModal(userId);
    };
    //open user model
    this.openUserModal = function (userId) {
        GetData("/admin/usermanager/edit", openModalSuccess, { formId: "frmAddNewUser", userId: userId });
    };
    // open  user success method 
    this.updateUserSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            closeCommonModel();
            refreshDataTable(tblUser);
        }
    };
    //delete user success
    this.deleteUserSuccess = function (result) {
        if (result) {
            SwalSuccess("Record deleted successfully!");
            refreshDataTable(tblUser);
        }
        else {
            SwalError("Something went wrong!");
            refreshDataTable(tblUser);
        }
    };
    // delete user
    this.deleteUser = function (userId) {
        swal({
            title: "Are you sure?",
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
            .then((willDelete) => {
                if (willDelete) {
                    PostData("/admin/usermanager/delete", this.deleteUserSuccess, { userId: userId });
                }
            });
    };

    //#endregion User

    //#region  Role
    this.roleListView = function () {
        $('#' + tblRole).dataTable({
            "bStateSave": false,
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            cache: true,
            responsive: true,
            "bServerSide": false,
            "bProcessing": true,
            "bJqueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[0, "asc"]],
            "sAjaxSource": "/admin/usermanager/getroles/",
            "preDrawCallback": function () {
            },
            "rowCallback": function (nRow, data) {
            },
            "drawCallback": function () {
                $("a.edit").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Edit Role'
                });
                $("a.edit").removeAttr("title");
                $("a.delete").tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: 'Delete Role'
                });
                $("a.delete").removeAttr("title");
            },
            "aoColumns": [
                { "mDataProp": "name", "sClass": "center" },

                {
                    "orderable": false,
                    "mRender": function (data, type, full) {
                        return "<a  class='edit' onClick=UserManagerViewModel.updateRole('" + full.id + "') ><i class='far fa-edit text-primary'></i></a> " +
                            "<a class='delete' onClick=UserManagerViewModel.deleteRole('" + full.id + "') )><i class='fas fa-trash-alt text-danger'></i></a>";
                    }
                }
            ],
            "autoWidth": true
        });
    };
    // open  update Role modal with data
    this.updateRole = function (roleId) {
        this.openRoleModal(roleId);
    };
    //open Role model
    this.openRoleModal = function (roleId) {
        GetData("/admin/usermanager/editrole", openModalSuccess, { formId: "frmAddNewRole", roleId: roleId });
    };
    // open  Role success method 
    this.updateRoleSuccess = function (success, message) {
        if (success === false) {
            failAlert(message);
        }
        else {
            successAlert(message);
            closeCommonModel();
            refreshDataTable(tblRole);
        }
    };
    // delete role
    this.deleteRole = function (roleId) {
        swal({
            title: "Are you sure?",
            text: "Role will be unassigned for currently assigned user.",
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
            .then((willDelete) => {
                if (willDelete) {
                    PostData("/admin/usermanager/deleterole", this.deleteRoleSuccess, { roleId: roleId });
                }
            });
    };
    //delete Role success
    this.deleteRoleSuccess = function (result) {
        if (result) {
            SwalSuccess("Record deleted successfully!");
            refreshDataTable(tblRole);
        }
        else {
            SwalError("Something went wrong!");
            refreshDataTable(tblRole);
        }
    };

    //#endregion Table Role

};