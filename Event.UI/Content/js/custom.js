﻿/***************************************Summer text file upload  Loader ***************************************/
//Dropzone Configuration
Dropzone.autoDiscover = false;
var fileUploadId = "";
// use for start show  loader on image upload
function showAjaxLoader() {
    $.blockUI({
        baseZ: 2000,
        message: '<h5 id="waitLoader"><img src="/images/loader.png" height="50px" /> Processing.Thank you for your patience.</h5> '
    });
}

//use for stop show loader on image upload
function stopAjaxLoader() {
    $.unblockUI();
}
/***************************************Summer text file upload  Loading ***************************************/


function failAlert(message) {
    // Display an error toast, with a title
    toastr.error(message, 'Fail');
}

function successAlert(heading, message) {
    // Display a success toast, with a title
    toastr.success(message, heading);
}

function warningAlert(heading, message) {
    // Display a success toast, with a title
    toastr.warning(message, heading);
}
/******************************************** Start  Swal Alert ************************************************/

function SwalSuccess(message) {
    swal(message, {
        icon: "success"
    });
}
//
function SwalError(message) {
    swal(message, {
        icon: "error"
    });
}
//
function swalConfirmBeforeAction(title, url, onSuccessMethod, data) {
    swal({
        title: title,
        icon: "warning",
        buttons: true,
        dangerMode: true
    })
        .then((willDelete) => {
            if (willDelete) {
                PostData(url, onSuccessMethod, data);
            }
        });
}

//#region
/******************************************** Start  ladda spinn wheel ************************************************/

function startLaddaSpinWheel(clickedButonId) {
    var btnLadda = $("#" + clickedButonId).ladda();
    btnLadda.ladda('start');
}

function stopLaddaSpinWheel(clickedButonId) {
    var btnLadda = $("#" + clickedButonId).ladda();
    btnLadda.ladda('stop');
}

/******************************************** End ladda spinn wheel ************************************************/
//#endregion


//#region
/************************************** *Common Js Method for Ajax  Start  ***************************************/

function PostData(url, _successHandler, data, showBlackImage, spinWheelButtonId, colorChartId, extraInfo) {

    var isSpinWheelButtonExist = false;
    if (showBlackImage) {
        showAjaxLoader();
    }
    if (extraInfo !== null && extraInfo !== undefined && extraInfo !== "") {
        $(".modal-hidden-input").val(extraInfo);
    }
    if (spinWheelButtonId !== null && spinWheelButtonId !== undefined && spinWheelButtonId !== "") {
        isSpinWheelButtonExist = true;
        startLaddaSpinWheel(spinWheelButtonId);
    }
    if (colorChartId !== null && colorChartId !== undefined && colorChartId !== "") {
        ColorChartId = colorChartId;
    }
    $.ajax({
        type: "POST",
        url: baseUrl + url,
        data: data,
        success: function successHandler(result) {
            if (isSpinWheelButtonExist) {
                stopLaddaSpinWheel(spinWheelButtonId);
            }
            if (showBlackImage) {
                stopAjaxLoader();
            }

            _successHandler(result, data);
        },        
        error: function () {
            //your code here
            if (showBlackImage) {
                stopAjaxLoader();
            }
        }

        //global: showBlackImage
    });
}

//
function GetData(url, _successHandler, data, showBlackImage, spinWheelButtonId) {
    var isSpinWheelButtonExist = false;
    if (spinWheelButtonId !== null && spinWheelButtonId !== undefined && spinWheelButtonId !== "") {
        isSpinWheelButtonExist = true;
        startLaddaSpinWheel(spinWheelButtonId);
    }
    if (showBlackImage === null) {
        showBlackImage = true;
    }
    $.ajax({
        type: "GET",
        url: baseUrl + url,
        data: data,
        success: function successHandler(result) {
            if (isSpinWheelButtonExist) {
                stopLaddaSpinWheel(spinWheelButtonId);
            }
            _successHandler(result, data);
        },
        global: showBlackImage
    });
}

/************************************** *Common Js Method for Ajax  End   ***************************************/
//#endregion


function ResetUnobtrusiveValidation(form) {
    form.removeData('validator');
    form.removeData('unobtrusiveValidation');
    $.validator.unobtrusive.parse(form);

}
/************************************** *Bootstrap Model  start ***************************************/

function openCommonModel(data, formId) {
    $("#common-modal-pop-up").html(data);


    //$("#common-modal-pop-up").modal('show');

    $('#common-modal-pop-up').modal({
        backdrop: 'static',
        keyboard: false
    }).show();

    ResetUnobtrusiveValidation($("#" + formId));
    //get the validator setting from form and if ignoreHidden=true add this code
    var igonreSettingsValue = ($("#" + formId).data("ignore-settings"));
    if ((igonreSettingsValue !== "" || igonreSettingsValue !== "undefined" || igonreSettingsValue !== null)) {
        if (igonreSettingsValue === true) {
            $("#" + formId).data("validator").settings.ignore = "";
        }
    }
}




function closeCommonModel() {
    $("#common-modal-pop-up").modal('hide');
}

function refreshDataTable(tableId) {
    if (tableId !== null && tableId !== undefined && tableId !== "") {
        //second parameter false will keep user on the same page instead of first after load in pagination.
        $('#' + tableId).DataTable().ajax.reload(null, false);
    }
}

//success method 
function openModalSuccess(result, data) {
    openCommonModel(result, data.formId);
    InitAjaxFormPost(data.formId);
}


//#region initPartialViewRender
/***************************************** Button Partial call Feature Starts *****************************************/
function initPartialViewRender() {
    $("a[data-load-partial-view='true']").on('click', function () {
        var data = $(this).data();
        // EVERY TIME RELOAD DATA 
        if (data.reloadDom || $("#" + data.domAppendIdentifier).children().length === 0) {
            GetData(data.url, partialViewRenderSuccess, { domAppendIdentifier: data.domAppendIdentifier });
        }
        else { //         
            $("#" + data.domAppendIdentifier).siblings().hide();
            $("#" + data.domAppendIdentifier).show();
        }

    });
}
//success method 
function partialViewRenderSuccess(result, data) {
    // add html and show 
    $("#" + data.domAppendIdentifier).empty().html(result).tab('show');

    $("#" + data.domAppendIdentifier).siblings().hide();
    $("#" + data.domAppendIdentifier).show();

    ///$("#common-modal-pop-up").modal('show');
    // ResetUnobtrusiveValidation($("#" + formId));
}
/***************************************** Button Partial call Feature Ends *****************************************/

//#endregion initPartialViewRender


/***************************************** DropDown Select2 Feature Starts *****************************************/
var config = {
    placeholder: 'Select an option',
    width: 'resolve'
};
//
function InitCustomDropdown() {
    $("select.dropDownSelect2").select2(config);
}

function InitCasCadingDropdown() {
    $("select[data-select-parent='true']").on('change', function () {
        var parentSelction = $(this);
        var url = parentSelction.data("child-data-url");
        var selectedId = parentSelction.data("select-parent-id");
        var childSelector = parentSelction.data("child-selector");
        // post data 
        var Id = parseInt($('#' + selectedId).val());
        //check is Id null
        if (isNaN(Id)) {  Id = -1; }
        $.ajax({
            url: baseUrl + url + Id,
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (response) {
                //if the child is stateId then need to change the region dropdown selction also
                if (childSelector == "StateId" || childSelector == "stateid") {
                    //first is their any region dropdown exist or not
                    var childOfchilSelector = $("#" + childSelector).data("child-selector");
                    if (childOfchilSelector != null && childOfchilSelector != undefined && childOfchilSelector != "") {
                        var newOptions = '<option value="">Please select region</option>';
                        $("#" + childOfchilSelector).select2('destroy').html(newOptions).prop("disabled", false)
                            .select2(config);
                    }
                }
                ReInitCustomDropdownById(childSelector, response.data);
                return false;
            },
            failure: function (result) {
            },
            error: function (error) {
                console.log(error);
                alert("Error getting subcategories" + error.statusText);
                $('.result').html(error.responseText);
            }
        });

    });
}
//
function InitCustomDropdownById(id) {
    $("#" + id).select2(config);
}
//accept list of select
function ReInitCustomDropdownById(id, data) {
    var newOptions = '';
    $.each(data, function (key, value) {
        newOptions += '<option value="' + value.Value + '">' + value.Text + '</option>';
    });
    $("#" + id).select2('destroy').html(newOptions).prop("disabled", false)
        .select2(config);
}


/***************************************** DropDown Select2 Feature Ends *****************************************/
/********Document Ready function Starts********/

$(document).ready(function () {
    InitilizeDatePicker();
    InitilizeTimePicker();
    InitCustomDropdown();
    initPartialViewRender();
    InitCasCadingDropdown();
    InitAjaxFormPost();
    InitilizeLightbox();
    //InitAjaxFormWithFilesPost();
    MainViewModel.setAdminTimeZoneOffset();
    //MainViewModel.InitializeDeletionButton();
    InitSummerNoteFileUpload();

});
/********Document Ready function Ends********/

/************************************** *AJAX POST Starts***************************************/

function InitAjaxFormPost(formId) {
    var finder = $("form[data-post-type='ajax'][ method=\"post\"]").find(':submit');
    if (formId !== undefined) {
        finder = $("#" + formId).find(':submit');
    }
    finder.on("click", function () {
        var button = $(this);
        var form = $(this).parents('form:first');
        var successFunction = form.data("success-method");
        var resetForm = form.data("clear-form");
        var isValid = form.validate().form();
        // check for ladda button option
        var laddaButton = form.data("ladda-button");
        debugger
        if (laddaButton === "" ||
            laddaButton === "undefined" ||
            laddaButton === null ||
            laddaButton === undefined) {
            laddaButton = button.attr('id');
        }
        if (isValid) {
            startLaddaSpinWheel(laddaButton);
            //form.css("opacity", "0.2");
            $(button).attr("disabled", true);
            $.ajax({
                type: 'POST',
                url: baseUrl + form.attr('action'),
                data: form.serialize(),
                success: function (data) {
                    //form.css("opacity", "1");
                    $(button).removeAttr("disabled");
                    if (resetForm === true && (data.success === undefined || data.success || data.success === null)) {
                        (form).trigger("reset");
                    }
                    if (trim(successFunction) !== "") {
                        var functionName = successFunction + "(" + data.success + " , '" + data.message + "' )";
                        eval(functionName);
                    }
                    //stop wheel
                    stopLaddaSpinWheel(laddaButton);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    failAlert(errorThrown);
                    //stop wheel
                    stopLaddaSpinWheel(laddaButton);
                }
            });
            return false;
        }
        return false;
    });
}

/************************************** *AJAX POST Ends***************************************/


/************************************** *AJAX POST  with file upload starts***************************************/

function InitAjaxFormWithFilesPost() {
    $("form[data-post-type-with-fileUpload='ajax'][ method=\"post\"]").find(':submit').on("click", function () {
        var button = $(this);
        var form = $(this).parents('form:first');
        var successFunction = form.data("success-method");
        var resetForm = form.data("clear-form");
        var apiPost = form.data("api-post");
        var url = form.attr('action');
        //if it is api post request
        if (apiPost !== null && apiPost !== undefined && apiPost) {
            url = apiEndPoint + url;
        }
        var isValid = form.validate().form();
        if (isValid) {
            //form.css("opacity", "0.2");
            $(button).attr("disabled", true);
            var fd = new FormData();
            var totalFiles = $('input[type="file"]').length;
            if (totalFiles > 0) {
                for (var inputFile = 0; inputFile < totalFiles; inputFile++) {
                    var file_data = $('input[type="file"]')[inputFile].files; // for multiple files
                    for (var i = 0; i < file_data.length; i++) {
                        fd.append("file_" + inputFile, file_data[i]);
                    }
                }
            }
            var other_data = form.serializeArray();

            $.each(other_data, function (i, field) {
                if (field.name === "fileUploaded") {
                    //do something
                }
                else {
                    fd.append(field.name, field.value);
                }
            });
            $.ajax({
                type: 'POST',
                url: baseUrl + url,
                data: fd,
                processData: false,
                contentType: false,
                success: function (data) {
                    //form.css("opacity", "1");
                    $(button).removeAttr("disabled");
                    if (resetForm === true && (data.success === undefined || data.success || data.success === null)) {
                        (form).trigger("reset");
                    }
                    if (trim(successFunction) !== "") {
                        var functionName = successFunction + "(" + data.success + " , '" + data.message + "','" + data.IsResultSuccessfull + "' )";
                        eval(functionName);
                    }
                    //stop wheel
                    stopLaddaSpinWheel(button.attr('id'));
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    failAlert(errorThrown);
                    //stop wheel
                    stopLaddaSpinWheel(button.attr('id'));
                }
            });
            return false;
        }
        else {
            $(form).effect("shake", { distance: 6, times: 2 }, 20);
        }
        return false;
    });
}
/************************************** *AJAX POST  with file upload ends***************************************/


/************************************** *Trim Function Starts***************************************/

function trim(stringToTrim) {
    if (!isStringValid(stringToTrim)) return "";
    return stringToTrim.replace(/^\s+|\s+$/g, "");
}
function ltrim(stringToTrim) {
    if (!isStringValid(stringToTrim)) return "";
    return stringToTrim.replace(/^\s+/, "");
}
function rtrim(stringToTrim) {
    if (!isStringValid(stringToTrim)) return "";
    return stringToTrim.replace(/\s+$/, "");
}
function isStringValid(str) {
    if (str === "") return false;
    if (str === undefined) return false;
    return true;
}
/************************************** *Trim Function Ends***************************************/
function InitSummerNoteFileUpload() {
    $('.summernote-fileupload').each(function () {
        var editor =  // 1st change: will need this variable later
            $(this).summernote({
                height: 100,
                focus: false,
                callbacks: { // 2nd change - onImageUpload inside of "callbacks"
                    onImageUpload: function (files) {
                        // 3rd change - dont need other params
                        var formData = new FormData();
                        formData.append("file", files[0]);
                        //start summer Note File Upload Loading
                        showAjaxLoader();
                        $.ajax({
                            url: baseUrl + $(this).data('url'),
                            data: formData,
                            type: 'POST',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (imageUrl) {
                                if (!imageUrl) {
                                    // stop Summer Note File Upload Loading
                                    stopAjaxLoader();
                                    return;
                                }
                                // 4th change - create img element and add to document
                                $.each(imageUrl.message, function (index, value) {
                                    resultUrl = value.url;
                                    stopAjaxLoader();
                                });

                                var imgNode = document.createElement('img');
                                imgNode.src = resultUrl;
                                editor.summernote('insertNode', imgNode);
                            },
                            error: function () {
                                // stop Summer Note File Upload Loading
                                stopAjaxLoader();
                            }
                        });
                    }
                }
            });
    });
}

//method to initialize datePicker
function InitilizeDatePicker() {
    $('.couponfactoryDatePicker').each(function () {
        // if future date is not allow
        if ($(this).attr("data-allow-future-date") === "false") {
            $(this).datepicker({ maxDate: new Date() });
        }

        // if current control has not value set current date as value
        if ($(this).val() !== "" && $(this).data('setcurrentDate') !== null && $(this).data('setcurrentDate') === 'true') {
            $(this).datepicker().datepicker("setDate", new Date());
        }
        if ($(this).data('setcurrentdate') === true) {
            $(this).datepicker().datepicker("setDate", new Date());
        }
        $(this).datepicker(
            {
                autoclose: true,
                format: 'm/d/yyyy',
            }
        );
    });
}


//method to initialize Time Picker
function InitilizeTimePicker() {
    $('#chinaMountain-timepicker').each(function () {
        $(this).datetimepicker({
            locale: 'ru'
        });
        //Bootstrap Duallistbox
        $('.duallistbox').bootstrapDualListbox();
    });
}




//method to initialize lightbox
function InitilizeLightbox() {
    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox({
            alwaysShowClose: true,
            selector: ".child__element"
        });
    });
}

/***********************************************Convert standard time to military time******************************************************/

function convertStandardToMilitaryTime(control) {
    $getFieldId = control.id;
    var time = $("#" + $getFieldId).val();
    if (time !== "" && time !== undefined) {
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if (AMPM === "PM" && hours < 12) hours = hours + 12;
        if (AMPM === "AM" && hours === 12) hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10) sHours = "0" + sHours;
        if (minutes < 10) sMinutes = "0" + sMinutes;
        convertedStandardTime = sHours + ":" + sMinutes + ":" + "00";
        $("#" + $getFieldId).siblings(".store-timePicker-value").val(convertedStandardTime);
    }
}


/*method for generating PDF*/
//save bytes of generated pdf
function saveByteArray(reportName, byte) {
    var blob = new Blob([byte], { type: "application/pdf" });
    var link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    var fileName = reportName;
    link.download = fileName;
    link.click();
}

// convert base64 to array Buffer sent by server
function base64ToArrayBuffer(base64String) {
    var binaryString = window.atob(base64String);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
        var ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes;
}

//get formatted time
function getFormattedTime() {
    var today = new Date();
    var y = today.getFullYear();
    // JavaScript months are 0-based.
    var m = today.getMonth() + 1;
    var d = today.getDate();
    var h = today.getHours();
    var mi = today.getMinutes();
    var s = today.getSeconds();
    return y + "-" + m + "-" + d + "-" + h + "-" + mi + "-" + s;
}

/***********************************************Phone validation******************************************************/

function convertPhoneToDashedNumber(control) {
    // get phone number
    var inputValue = $(control).val();
    // remove hyphen in phone number
    inputValue = inputValue.replace(/-/g, "");
    // regex for UK and check UK validate numbers
    var regexUK = /^((\+44\s?\d{4}|\(?\d{5}\)?)\s?\d{6})|((\+44\s?|0)7\d{3}\s?\d{6})$/;
    // check if UK phone number is not valid and length of phone number is greater than 9
    if (!inputValue.match(regexUK) && inputValue.length > 9) {
        //place hyhpen in phone number
        inputValue = inputValue
            .match(/\d*/g).join('')
            .match(/(\d{0,3})(\d{0,3})(\d{0,4})/).slice(1).join('-')
            .replace(/-*$/g, '');
    }
    $(control).val(inputValue);
}

/***********************************************Select price input text******************************************************/

function selectPriceInputText(control) {
    if ($(control).val() === 0.00) {
        $(control).select();
    }
}

/***********************************************full price format******************************************************/

function fullPriceFormat(control) {
    // take input value
    var inputValue = $(control).val();
    //split string input value into array
    var getInputArray = inputValue.split(".");
    // convert first array value to integer
    getInputArray[0] = parseInt(getInputArray[0]);
    // check first array value
    if (getInputArray[0] === "" || isNaN(getInputArray[0]) || getInputArray[0] === null || getInputArray[0] === 0) {
        getInputArray[0] = "0";
    }
    // check second array value 
    if (getInputArray[1] === "" || getInputArray[1] === "undefined" || getInputArray[1] === null || getInputArray[1] === 0) {
        getInputArray[1] = "00";
    }
    //check length of second array value
    if (getInputArray[1].length === 1) {
        getInputArray[1] = getInputArray[1] + "0";
    }
    // place converted values in input field
    $(control).val(getInputArray[0] + "." + getInputArray[1]);
}


/**********************************Hide button************************************/

function hideButton(identifier) {
    $(identifier).hide();
}


/************************ prevent form button to submit when enter press**************/
function preventFormSubmitOnEnterPress() {
    // prevent enter key
    $(window).on('keyup keypress', function (event) {
        //check submit button focused or not
        var hasFocus = $(".save-btn").is(':focus');
        if (!hasFocus) {
            if (event.keyCode === 13) {
                event.preventDefault();
                return false;
            }
        }
    });
}

//Sort dropdowns client side.
function sortSelect(selector, skipFirst) {
    var options = (skipFirst) ? $(selector + ' option:not(:first)') : $(selector + ' option');
    var arr = options.map(function (_, o) {
        return {
            t: $(o).text(), v: o.value, s: $(o).prop('selected')
        };
    }).get();
    arr.sort(function (o1, o2) {
        var t1 = o1.t.toLowerCase(), t2 = o2.t.toLowerCase();
        return t1 > t2 ? 1 : t1 < t2 ? -1 : 0;
    });
    options.each(function (i, o) {
        o.value = arr[i].v;
        $(o).text(arr[i].t);
        if (arr[i].s) {
            $(o).attr('selected', 'selected').prop('selected', true);
        } else {
            $(o).removeAttr('selected');
            $(o).prop('selected', false);
        }
    });
}
//-------------------------------------- Spin  On every Ajax request in the System -------------------

function spinOn() {
    $("#spinner").show();
}

function spinOff() {
    $("#spinner").hide();
}

$(document).ajaxSend(function () {
    spinOn();
});

$(document).ajaxStop(function () {
    spinOff();
});



//method to initialize fileuploader
function InitilizefileUploader() {
    $('.couponfactory-fileUpload').each(function () {
        console.log(this.id);
        //Date picker
        var data = $(this).data();
        var destinationControl = data.destinationControl;
        new Dropzone(
            '#' + this.id,
            {
                url: baseUrl+data.url, // Set the url
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 110, // MB
                clickable: true,
                maxFiles: 1,
                acceptedFiles: "audio/*,image/*,.psd,.pdf,.rar,.mov",
                enqueueForUpload: false,
                //addRemoveLinks: true,
                previewsContainer: "#" + this.id,
                //dictRemoveFile: " Trash",
                init: function () {
                    this.on("processing", function (file) {
                    });
                    this.on("maxfilesexceeded",
                        function (file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        });
                    this.on("success",
                        function (file, responseText) {
                            var resultUrl;
                            $.each(responseText.message, function (index, value) {
                                resultUrl = value.url;
                            });
                            if (destinationControl !== null && destinationControl !== undefined && destinationControl !== "") {
                                $("#" + destinationControl).val(resultUrl);
                            }
                            // check for destinATION CONTROL AND ADD THE VALUE TO IT
                            // toastr.success('Picture uploaded successfully!', 'Success!');
                        });
                    this.on("error",
                        function (data, errorMessage, xhr) {
                            toastr.error(errorMessage, 'Error!');
                        });
                }
            });

    });
}

//method to initialize datePicker
function InitilizefileUploaderById(id) {
    //debugger
    //console.log(id);
    var data = $("#" + id).data();
    var destinationControl = data.destinationControl;
    var purpose = data.purpose;
    var attachmentConatiner = data.attachmentContainer;
    new Dropzone(
        '#' + id,
        {
            url: baseUrl + data.url, // Set the url
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 10, // MB
            clickable: true,
            maxFiles: 1,
            acceptedFiles: 'image/*',
            enqueueForUpload: false,
            //addRemoveLinks: true,
            previewsContainer: "#" + id,
            //dictRemoveFile: " Trash",
            init: function () {
                this.on("processing", function (file) {
                });
                this.on("maxfilesexceeded",
                    function (file) {
                        this.removeAllFiles();
                        this.addFile(file);
                    });
                this.on("success",
                    function (file, response) {
                        //debugger
                        //bind an html to show a link and delete button below thw file uploader
                        if (response.success) {
                            var msgArray = response.message.split(',');

                            if (destinationControl !== null && destinationControl !== undefined && destinationControl !== "") {
                                $("#" + destinationControl).val(msgArray[0]);
                            } 


                            $("#" + id+" .dz-message").css("display", "none");
                            //make the file uploader area non-clickable
                            $("#" + id).css({ "pointer-events": "none", "cursor": "default" });
                            //show the image for new and update screen
                            var attachmentHtml = '<div class="row margin-left-0 margin-right-0 margin-bottom-5 margin-top-10" id="attachmentFile' + msgArray[0] + '"> <div class="col-xl-10 col-md-8 col-xs-8 text-left"><i class="fa fa-file text-success"></i><a class="title attachmentFile" data-attachment-id=' + msgArray[0] + ' onclick="ImagePreview(this)">' + msgArray[1] + '</a></div ><div class="col-xl-2 col-md-4 col-xs-4 text-right"><a class="btn btn-default" id="attachmentFile-' + msgArray[0] + '" data-attachment-controlId=' + destinationControl + ' style="cursor:pointer;" onclick="DeleteSingleAttachment(this)" data-destination-control=' + id + ' data-purpose=' + purpose + ' data-name="icon." data-attachment-id=' + msgArray[0] + ' title="delete file"><i class="fas fa-trash-alt text-danger"></i></a></div></div >';
                            $("#" + attachmentConatiner + " div").addClass("d-none");
                            $("#" + attachmentConatiner).append(attachmentHtml);
                        }
                        else {
                            toastr.error("Something Went Wrong!", 'Error!');
                        }
                       
                        // check for destinATION CONTROL AND ADD THE VALUE TO IT
                        // toastr.success('Picture uploaded successfully!', 'Success!');
                    });
                this.on("error",
                    function (data, errorMessage, xhr) {
                        toastr.error(errorMessage, 'Error!');
                    });
            }
        });

}

/* show the image*/
function ImagePreview(controlElement) {
    //debugger
    var data = $(controlElement).data();
    if (data.attachmentId == null || data.attachmentId == undefined || data.attachmentId == "") {
        data.attachmentId = "0";
    }
    window.open(baseUrl + '/attachment/ImagePreview?id=' + data.attachmentId);
}

/* delete selected attachment/uploaded file */
function DeleteSingleAttachment(controlElement) {
    //debugger
    var data = $(controlElement).data();
    if (data.destinationControl != null && data.destinationControl != undefined && data.destinationControl != "") {
        fileUploadId = data.destinationControl;
    }
    swal({
        title: "Are you sure?",
        text: "Please confirm you wish to delete " + data.name,
        icon: "warning",
        buttons: true,
        dangerMode: true
    })
        .then((willDelete) => {
            if (willDelete) {
                //debugger
                if (data.attachmentId == undefined || data.attachmentId == null || data.attachmentId == "") {
                    data.attachmentId = "0";
                }
                PostData("/attachment/deleteattachmentbyid", DeleteSingleAttachmentSuccess, { attachmentId: data.attachmentId });
            }
        });
}

/*success method for deleting single attachment */
function DeleteSingleAttachmentSuccess(success, message) {
    //debugger
    if (success == "true" || success == "True") {
        if (fileUploadId != "" && fileUploadId != null && fileUploadId != undefined) {
            var attachmentId = $("#" + fileUploadId).attr("data-destination-control");
            //delete it's id from attachmentId control
            if (attachmentId != null && attachmentId != undefined && attachmentId != "") {
                $("#" + attachmentId).val("");
            }

            //remove all the instance of the selected fileUploader and re-initialize it
            $.each(Dropzone.instances, function (index, value) {
                if (value.element.id == fileUploadId) {
                    //destroy 
                    value.destroy();
                    //reinitialize 
                    InitilizefileUploaderById(value.element.id)
                }
            });

            //show the drag and drop text 
            $("#" + fileUploadId).removeAttr("style");
            SwalSuccess("File Deleted Successfully!");
            $("#attachmentFile" + message.attachmentId).remove();
            //show the drag and drop option
            $("#" + fileUploadId + " .dz-message").css("display", "block");
        }
        else {
            SwalError("Something Went Wrong!");
        }
    }
    else {
        SwalError("Something Went Wrong!");
    }
}

/*****************prevent the entire document from drag and drop files*********************/
function preventDragAndDropFeatures() {
    $(document).bind('drop dragover', function (e) {
        e.preventDefault();
    });
}

//method to initialize datePicker
function InitilizeMultifileUploader() {
    $('.chinaMountain-multifileUpload').each(function () {
        //Date picker
        var data = $(this).data();
        new Dropzone(
            '#' + this.id,
            {
                url: baseUrl+ data.url, // Set the url
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 100, // MB
                clickable: true,
                maxFiles: 10,
                acceptedFiles: 'image/*',
                enqueueForUpload: false,
                addRemoveLinks: true,
                init: function () {
                    this.on("processing", function (file) {
                    });
                    this.on("maxfilesexceeded",
                        function (file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        });
                    this.on("success",
                        function (file, responseText) {
                            //debugger
                            var currentControl = $(this.element).attr('id');
                            var fileCounter = parseInt($("#" + currentControl + "-MediaItemListCount").val());
                            var addfileCounter = fileCounter + 1;
                            $("#" + currentControl + "-MediaItemListCount").val(addfileCounter);
                            $.each(responseText.message, function (index, value) {
                                //fileCounter = fileCounter + 1;
                                var attachmentDiv = mapFileuploaderHTML(fileCounter, value.name, value.url);
                                $("#" + currentControl + "-MediaItemList").append(attachmentDiv);
                            });
                            //$("#" + currentControl + "-MediaItemListCount").val(fileCounter++);
                            this.removeAllFiles();
                        });
                    this.on("error",
                        function (data, errorMessage, xhr) {
                            toastr.error(errorMessage, 'Error!');
                        });
                }
            });

    });
}

//This will return the html for file uploader 
function mapFileuploaderHTML(counter, fileName, location) {
    //debugger
    //get the attachment List and show one by one 
    return '<div class="row mb-2" id="attachmentFile' + counter + '"> ' +
        '<input class="AttachmentMediaMappingId"  id="MediaItemList_' + counter + '__SaveLocation" name="MediaItemList[' + counter + '].SaveLocation" type="hidden" value="' + location + '"> ' +
        '<input class="AttachmentMediaMappingId"  id="MediaItemList_' + counter + '__Id" name="MediaItemList[' + counter + '].Id" type="hidden" value="0"> ' +
        '<input class="MediaItemId" data-val="true" data-val-required="The IsDeleted field is required." id="MediaItemList_' + counter + '__IsDeleted" name="MediaItemList[' + counter + '].IsDeleted" type="hidden" value="False">' +
        '<div class="col-xl-8 col-md-6 col-xs-6 text-left"> <i class="fa fa-file text-success"></i> ' +
        '<a class="title" target="_blank" href="' + location + '">' + fileName + '</a></div><div class="col-xl-4 col-md-6 col-xs-6 text-right">' +
        '<a class="btn btn-sm btn-danger" id="attachmentFile-' + counter + '"  style="cursor:pointer;"   title="delete file" data-deleteattachmentbutton="true" onclick="AttachmentViewModel.deletedSelectedAttachmentFile(' + counter + ')"><i class="fa fa-trash-o fa-1x"></i></a></div></div>';
}


//allow only numeric value for value
$(".allow_numeric").on("keypress keyup blur", function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }
});

//allow only numeric value for value
$(".allow_decimal").on("keypress keyup blur", function (e) {
    //if the letter is not digit then display error and don't type anything
    if ((event.which !== 46 || $(this).val().indexOf('.') !== -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
});

// set decimal text box default value  0.00
$(".setDefaultDecimalValue").each(function (e) {
    if ($(this).val() === 0 || $(this).val() === '0' || $(this).val() === null
        || $(this).val() === undefined
        || $(this).val() === 'undefined' || $(this).val() === '') {
        $(this).val('0.00');
    }
});

