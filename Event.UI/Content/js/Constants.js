﻿var ConstantViewModel = new function () {
   // var thisViewModel = this;
    this.constants = new Object({
        TIMEZONE_HEADER_NAME: "TimeZoneOffset",
        STORE_DATE_FORMAT_FOUR_DIGITS_YEAR: 'MM/DD/YYYY',
        STORE_DATE_TIME_FORMAT_FOUR_DIGITS_YEAR: 'MM/DD/YYYY h:mm:ss A',
        US_DATE_FORMAT: 'MM/DD/YY',
        US_DATE_AND_TIME_FORMAT: 'MM/DD/YY h:mm A',
        US_DATE_AND_TIME_FORMAT_WITH_BOTH_HOUR_DIGITS: 'MM/DD/YY hh:mm A',
        US_DATE_AND_TIME_WITHOUT_YEAR_AND_SECONDS_FORMAT: 'MM/DD h:mm A',
        US_DATE_AND_TIME_WITH_MILLISECONDS_FORMAT: 'MM/DD/YY h:mm:ss.SS A',
        US_TIME_FORMAT: 'h:mm:ss A',
        US_TIME_WITHOUT_SECONDS_FORMAT: 'h:mm A',
        US_TIME_WITH_MILLISECONDS_FORMAT: 'h:mm:ss.SS A',
        US_DATE_INPUT_PARSE_FORMAT: 'YYYY-MM-DD',
        US_DATE_AND_TIME_INPUT_PARSE_FORMAT: 'MM/DD/YYYY h:mm A',
        US_DATE_AND_TIME_SERVER_PARSE_FORMAT: "YYYY-MM-DDTHH:mm:ss.SSZ",
        US_TIME_SERVER_PARSE_FORMAT: "THH:mm:ss.SSZ",
        REG_EX_SEMICOLON_EMAIL_LIST: "^\\s*(([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5}){1,25}\\s*)+([;.]\\s*(([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5}){1,25})+)*\\s*;*$",
        REG_EX_INTEGER_SLASH_SEPARATED_LIST: "^(\\d+(\\/\\d+)*)?$",
        REG_EX_INTEGER_COMMA_SEPARATED_LIST: "^(\\d+((\\,\\d+)|(\\, \\d+))*)?$",
        REG_EX_INTEGER_SINGLE: "^([1-9]|[1-9][0-9]|[1-9][0-9][0-9])$",
        REG_EX_FIVE_DECIMAL_PLACES_MAX: "^[-+]?(?!0\\d|$)\\d*(\\.\\d{1,5})?$",
        REG_EX_NO_SPECIAL_CHARACTERS: "^[a-zA-Z0-9_]+$",
        INVALID_ID: -1,
        COLUMN_SEPARATOR: "&nbsp;&nbsp;",
        NOT_AVAILABLE_ACRONYM: 'N/A',
        NOT_SPECIFIED_ACRONYM: 'N/A',
        UNEXPECTED_ERROR_AJAX_CALL_MESSAGE: "An unexpected error occurred, please contact an administrator!",
        MONEY_SYMBOL: "$",
        THINK_IS_A_MISTAKE_MESSAGE: ", if you think this is a mistake please contact an administrator.",
        NO_SCHEDULE_FINISH_TIME: "There's no scheduled time of finish.",
        USAGE_UNIT_TEXT: "Usage unit",
        MAX_FILE_SIZE_IN_KBYTES: 20480,

        FILE_UPLOAD_DEFAULT_REGEX: /(.*?)/,
        FILE_UPLOAD_DEFAULT_ERROR: "The selected file must be a supported document format, such as a PDF (.pdf), Excel Spreadsheet (.xlsx), or Text (.txt) file.  For further questions, please contact an administrator.",
        FILE_UPLOAD_IMAGE_REGEX: /^image\/(?!tif)/,
        FILE_UPLOAD_IMAGE_ERROR: "The selected file must be in a supported image format, such as a JPEG, GIF, PNG, or BMP.  For further questions, please contact an administrator.",
        FILE_UPLOAD_HTML_REGEX: /^text\/html/,
        FILE_UPLOAD_HTML_ERROR: "The selected file must be an HTML file.",

    });

};

