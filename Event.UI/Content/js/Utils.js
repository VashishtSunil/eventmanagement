﻿var UtilViewModel = new function () {

    this.getMomentDateFromInput = function (dateAsString) {
        return moment(dateAsString, ConstantViewModel.constants.US_DATE_INPUT_PARSE_FORMAT);
    };
    this.getMomentDateAndTimeFromInput = function (dateAsString) {
        return moment(dateAsString, ConstantViewModel.constants.US_DATE_AND_TIME_INPUT_PARSE_FORMAT);
    };

    this.getMomentDateFromServer = function (dateAsString) {
        return moment.utc(dateAsString, ConstantViewModel.constants.US_DATE_AND_TIME_SERVER_PARSE_FORMAT);
    };

    this.getMomentDateAndTimeFromServer = function (dateAsString) {
        return moment.utc(dateAsString, ConstantViewModel.constants.US_DATE_AND_TIME_SERVER_PARSE_FORMAT);
    };
    this.getMomentDateFromDbDate = function (date) {
        var splitSlashDate = date.split('/Date(')[1];
        var correctDateAsString = splitSlashDate.split(')/')[0];
        var correctUnixDate = parseInt(correctDateAsString);
        return moment(correctUnixDate).format(ConstantViewModel.constants.STORE_DATE_FORMAT_FOUR_DIGITS_YEAR);
    };
    this.getMomentDateTimeFromDbDate = function (date) {
        var splitSlashDate = date.split('/Date(')[1];
        var correctDateAsString = splitSlashDate.split(')/')[0];
        var correctUnixDate = parseInt(correctDateAsString);
        return moment(correctUnixDate).format(ConstantViewModel.constants.STORE_DATE_TIME_FORMAT_FOUR_DIGITS_YEAR);
    };
    this.getDatePartFromDbDate = function (dateTimeAsString) {
        return dateTimeAsString.split(' ')[0];
    };

    this.getDatePartFromDbUTCDate = function (dateTimeAsString) {
        var date = new Date(dateTimeAsString);
        // if you want to get locale time, use
        return date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
    };

    this.getCurrentMoment = function (asUtc) {
        if (asUtc === true) {
            return moment.utc();
        } else {
            return moment();
        }
    };
    //format for input number as 0.00
    this.getPriceFormatFromInput = function (number) {
        if (!isNaN(number) && number.toString().indexOf('.') !== -1) {
            //if (Number.isInteger(number)) {
            return number.toFixed(2);
        }
        else {
            return parseFloat(number).toFixed(2);
        }
    };
    //show dot after 20 words
    this.getDescriptionFormatFromInput = function (inputAsHtml) {
        inputAsHtml = inputAsHtml[0].textContent;
        var format = ".......";
        if (inputAsHtml.indexOf("<") === (-1)) {
            return inputAsHtml;
        }
        else {
            var stringInput = $(inputAsHtml).text();
            //if input data is in html
            if (stringInput ===undefined) {
                if (stringInput.length > 200) {
                    return stringInput.substring(0, 200) + format;
                }
                return stringInput;
            }
            if (inputAsHtml.length > 200) {
                return inputAsHtml.substring(0, 200) + format;
            }
            return inputAsHtml;
        }
    };
};
    


