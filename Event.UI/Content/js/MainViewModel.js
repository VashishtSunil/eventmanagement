﻿var MainViewModel = new function () {
    var thisViewModel = this;
    this.BaseUrl;
    this.SetBaseUrl = function (baseUrl) {
        thisViewModel.BaseUrl = baseUrl;
    };

    this.GetBaseUrl = function () {
        return thisViewModel.BaseUrl;
    };

    this.SetServer = function () {

    };

    //SetAdminTimeZoneOffset
    this.setAdminTimeZoneOffset = function () {
        var dt = new Date();
        // PostData("/account/setadmintimezoneoffset?offsetmin=" + -dt.getTimezoneOffset(), this.setAdminTimeZoneOffsetSuccess);
        // this is been used in login page to set the cookies so we are not using custom js 
        $.ajax({
            type: "POST",
            url: "/account/setadmintimezoneoffset?offsetmin=" + -dt.getTimezoneOffset(),
            data: {}
        }).done(function (data) {

        });
    };

    //SetAdminTimeZoneOffset success
    this.setAdminTimeZoneOffsetSuccess = function (result, data) {

    };

    this.InitializeDeletionButtonById = function (Id) {
        $("#" + Id + "").btsConfirmButton("Delete item Now!",
            function (e) {
                var data = $(this).data();
                var identifier = data.identifier;
                var viewModelName = data.viewmodelname;
                var methodName = data.methodname;
                if (identifier !== null && identifier !== undefined) {
                    var functionName = viewModelName + "." + methodName + "('" + identifier + "')";
                    eval(functionName);
                }
            })
            .on('confirm:after', function (e) {
            }).on('confirm:before', function (e) {
            }).on('confirm:expired', function (e) {

            });
    };

    //this.InitializeDeletionButton = function () {
    //    $('.btn-del').btsConfirmButton("Delete item Now!",
    //        function (e) {
    //            var data = $(this).data();
    //            // alert($(this).hasClass("btn-danger"));
    //            if ($(this).hasClass("btn-danger")) {
    //                var identifier = data.identifier;
    //                //var delIdentifier = data.delIdentifier;
    //                //var isDatatable = data.isDatatable;
    //                // if (data.hasClass("btn-danger")) {
    //                var viewModelName = data.viewmodelname;
    //                var methodName = data.methodname;
    //                if (identifier !== null && identifier !== undefined) {
    //                    var functionName = viewModelName + "." + methodName + "('" + identifier + "')";
    //                    eval(functionName);
    //                }
    //            }
    //        })
    //        .on('confirm:after', function (e) {

    //        }).on('confirm:before', function (e) {
    //        }).on('confirm:expired', function (e) {

    //        });
    //};

    // HIDE ALL EXCEPT LAST 
    this.HideAllExceptLastButton = function (identifier) {
        $(identifier).last().find('.option-child').show();
        //  $(identifier).last().find('.option-child').css("display", "");
        $(identifier).last().find('.attribute-value').focus();
    };

    // show last button 
    this.CheckForLastButton = function (identifier) {
        $(identifier).last().find('.option-child').show();
    };

    //method to initialize fullcalendar
    this.InitializeFullCalendar = function (data) {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
            height: 'parent',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
            },
            defaultView: 'dayGridMonth',
            defaultDate: new Date(),
            navLinks: true, // can click day/week names to navigate views
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: data,
            timeFormat: 'H:mm',
            eventClick: function (info) {
                info.jsEvent.preventDefault(); // don't let the browser navigate

                if (info.event.url) {
                    location.href = info.event.url;     //window.open(info.event.url);
                }
            },
            eventMouseEnter: function (info) {
                $(info.el).tooltip({
                    title: "title : " + info.el.text + "",
                    placement: "top",
                    trigger: "hover",
                    container: "body"
                });
            },
            eventMouseLeave: function (data, jsEvent, view) {
                $('.tooltiptopicevent').remove();
            }

        });
        calendar.render();
    };

    //for multi file Upload

    this.MultiFielUpload = function () {
        $('.fileUpload').each(function () {
            $(this).fileupload({
                dropZone: $(this).parents("#dropzone"),
                url: "/order/UploadAttachmentFiles",
                dataType: 'json',
                disableImageResize: false,
                imageMaxWidth: 1200,
                imageMaxHeight: 1200,
                loadImageMaxFileSize: 30000000, //30 MB photo upload, we can increase more too if needed
                imageCrop: true, // Force cropped images,
                sequentialUploads: false,
                multipart: true
            }).on('fileuploadadd', function (e, data) {
                data.context = $('<div/>').appendTo('#files');
                $.each(data.files, function (index, file) {
                    var node = $("<p style='margin- top:5px;'/>");
                    node.appendTo(data.context);
                });
            }).on('fileuploadprocessalways', function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.error) {
                    node.append('<br>').append($('<span class="text-danger"/>').text(file.error));
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button').text('Upload').prop('disabled', !!data.files.error);
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                var attachmentSpanId = e.currentTarget.parentElement.id;
                $("#" + attachmentSpanId).siblings('#progress').removeClass("hidden");
                $("#" + attachmentSpanId).siblings('#progress').children().css(
                    'width',
                    progress + '%'
                );
            }).on('fileuploaddone', function (e, data) {
                var attachmentSpanId = e.currentTarget.parentElement.id;
                if (data.result.success) {
                    $.each(data.result.message, function (index, file) {
                        if (file != null && file != undefined && file != "") {
                            var uploadedAttachmentList = "<div class='row margin-left-0 margin-right-0 margin-bottom-5' id='attachmentFile" + file.Id + "'><div class='col-xl-8 col-md-6 col-xs-6 text-left'><i class='fa fa-file text-success'></i> <a class='title' target='_blank' href ='" + (file.SaveLocation + file.DummyFileName) + "'>" + file.OriginalFileName + "</a></div><div class='col-xl-4 col-md-6 col-xs-6 text-right'><a class='btn btn-red' id='attachmentFile-" + file.Id + "' data-deleteButton='true' style='cursor:pointer;' data-viewModelName='OrderAttachmentsViewModel' data-methodName='deletedSelectedAttachmentFile' data-del-identifier='" + file.Id + "' data-identifier='" + file.Id + "' data-confirm-msg='Confirm' title='delete file'> <i class='fa fa-trash-o fa-1x'></i></a></div></div> ";
                            $(".NotUploadedFile").remove();
                            $("#" + attachmentSpanId).parent().append(uploadedAttachmentList);
                            MainViewModel.InitializeDeletionButtonById("attachmentFile-" + file.Id);
                            $("#" + attachmentSpanId).siblings('#progress').addClass("hidden");
                        }
                    });
                }
                else {
                    var error = $('<p class="text-danger NotUploadedFile"/>').text(data.result.message);
                    $("#" + attachmentSpanId).parent().append(error);
                    $("#" + attachmentSpanId).siblings('#progress').addClass("hidden");
                }
            }).on('fileuploadfail', function (e, data) {
                $.each(data.files, function (index) {
                    var error = $('<span class="text-danger"/>').text('File upload failed.');
                    $(data.context.children()[index])
                        .append('<br>')
                        .append(error);
                });
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
        });
    };

    //Date Time Formate
    this.getMomentDateTimeFromDbDate = function (date) {
        var splitSlashDate = date.split('/Date(')[1];
        var correctDateAsString = splitSlashDate.split(')/')[0];
        var correctUnixDate = parseInt(correctDateAsString);
        return moment(correctUnixDate).format('MM/DD/YYYY h:mm:ss A');
    };
};

