﻿using Event.Data.Datamodels.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Event.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            IdentityConfig.RegisterIdentities();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            MigrateManagementContext();
        }



        /// <summary>
        /// Migrate Management Context
        /// </summary>
        private static void MigrateManagementContext()
        {
            try
            {
                System.Data.Entity.Database.SetInitializer(new System.Data.Entity.MigrateDatabaseToLatestVersion<EventContext, Event.Library.Migrations.Configuration>());
                var configuration = new Event.Library.Migrations.Configuration();
                configuration.AutomaticMigrationsEnabled = true;
                configuration.AutomaticMigrationDataLossAllowed = true;
                var migrator = new System.Data.Entity.Migrations.DbMigrator(configuration);
                migrator.Update();
            }
            catch (Exception ex)
            {
                //Log.Error(ex, "MigrateManagementContext");
            }
        }
    }
}
