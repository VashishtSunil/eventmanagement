﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Event.UI.Startup))]
namespace Event.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
