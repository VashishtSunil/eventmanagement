﻿#region Using
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using Event.Core.Enums;
using Event.Library.Models;
#endregion

namespace Event.Library.Helper
{
    public class UserManager : Microsoft.AspNet.Identity.UserManager<IdentityUser>
    {
        private static readonly UserStore<IdentityUser> UserStore = new UserStore<IdentityUser>();
        private static readonly UserManager Instance = new UserManager();


        private UserManager()
            : base(UserStore)
        {
        }

        public static UserManager Create()
        {
            // We have to create our own user manager in order to override some default behavior:
            //
            //  - Override default password length requirement (6) with a length of 4
            //  - Override user name requirements to allow spaces and dots
            var passwordValidator = new MinimumLengthValidator(4);
            var userValidator = new UserValidator<IdentityUser, string>(Instance)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true,
            };

            Instance.UserValidator = userValidator;
            Instance.PasswordValidator = passwordValidator;

            return Instance;
        }
        /// <summary>
        /// 
        /// </summary>
        public static void Seed()
        {

            var passwordHash = Instance.PasswordHasher.HashPassword("admin@123");

            // Make sure we always have at least the demo user available to login with
            // this ensures the user does not have to explicitly register upon first use
            var adminUser = new IdentityUser
            {
                Id = "6bc8cee0-a03e-430b-9711-420ab0d6a596",
                Email = "admin@event.com",
                UserName = "admin@event.com",
                PasswordHash = passwordHash,
                SecurityStamp = Guid.NewGuid().ToString("D")
            };

            UserStore.Context.Set<IdentityUser>().AddOrUpdate(adminUser);
            UserStore.Context.SaveChanges();

            var store = new RoleStore<IdentityRole>();
            var roleManager = new Microsoft.AspNet.Identity.RoleManager<IdentityRole>(store);

            if (!roleManager.RoleExists(UserRoles.Administrator.ToString()))
            {
                var role = new IdentityRole { Name = UserRoles.Administrator.ToString() };
                roleManager.Create(role);
            }
            Instance.AddToRole(adminUser.Id, UserRoles.Administrator.ToString());
        }

        /// <summary>
        /// Add or edit user 
        /// </summary>
        /// <param name="registerViewModel"></param>
        /// <returns></returns>
        public static RegisterViewModel UpdateIdentityUser(RegisterViewModel registerViewModel)
        {
            try
            {
                //initialing variable type of IdentityUser
                var programUser = new IdentityUser();

                //get user with Id
                var user = Instance.Users.Where(x => x.Id == registerViewModel.Id).FirstOrDefault();

                //check password  
                var passwordHash = registerViewModel.Password == "userpassword" ?
                                             user.PasswordHash :
                                             Instance.PasswordHasher.HashPassword(registerViewModel.Password);

                // add new user
                if (string.IsNullOrEmpty(registerViewModel.Id) && user == null)
                {
                    programUser.Id = Guid.NewGuid().ToString();
                    programUser.Email = registerViewModel.Email;
                    programUser.UserName = registerViewModel.Email;
                    programUser.PasswordHash = passwordHash;
                    programUser.SecurityStamp = Guid.NewGuid().ToString("D");
                }
                //update a user
                else
                {
                    programUser.Id = registerViewModel.Id;
                    programUser.Email = registerViewModel.Email;
                    programUser.UserName = registerViewModel.Email;
                    programUser.PasswordHash = passwordHash;
                    programUser.SecurityStamp = user.SecurityStamp;
                }

                registerViewModel.Id = programUser.Id;
                registerViewModel.Password = programUser.PasswordHash;
                UserStore.Context.Set<IdentityUser>().AddOrUpdate(programUser);
                UserStore.Context.SaveChanges();

                var store = new RoleStore<IdentityRole>();
                var roleManager = new RoleManager<IdentityRole>(store);

                if (!roleManager.RoleExists(registerViewModel.UserRole.ToString()))
                {
                    var role = new IdentityRole { Name = registerViewModel.UserRole.ToString() };
                    roleManager.Create(role);
                }
                Instance.AddToRole(programUser.Id, registerViewModel.UserRole.ToString());

                return registerViewModel;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Remove user and related Information 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static async Task<bool> DeleteUser(string userId)
        {
            var user = await Instance.FindByIdAsync(userId);
            if (user != null)
            {
                var logins = user.Logins;
                var rolesForUser = await Instance.GetRolesAsync(userId);

                foreach (var login in logins.ToList())
                {
                    await Instance.RemoveLoginAsync(login.UserId, new UserLoginInfo(login.LoginProvider, login.ProviderKey));
                }

                if (rolesForUser.Count() > 0)
                {
                    foreach (var item in rolesForUser.ToList())
                    {
                        // item should be the name of the role
                        var result = await Instance.RemoveFromRoleAsync(user.Id, item);
                    }
                }
                // remove user
                await Instance.DeleteAsync(user);
            }
            return true;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static IdentityUser GetUserByEmail(string email)
        {
            //get user with Id
            return Instance.Users.Where(x => x.Email == email).FirstOrDefault();

        }

    }
}