﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Library.Models
{
    public class BreadcrumbViewModel
    {
        public string PageTitle { get; set; }
        public string HomePageTitle { get; set; }
        public string HomePageUrl { get; set; }
        public string ChildPage { get; set; }
        public string ChildPageUrl { get; set; }
        public string CurrentPage { get; set; }
    }
}
