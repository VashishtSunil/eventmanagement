﻿#region Using
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
#endregion

namespace Event.Library.Utilities
{
    public static class Utilities
    {


        /// <summary>
        ///     Compares the requested route with the given <paramref name="value" /> value, if a match is found the
        ///     <paramref name="attribute" /> value is returned.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="value">The action value to compare to the requested route action.</param>
        /// <param name="attribute">The attribute value to return in the current action matches the given action value.</param>
        /// <returns>A HtmlString containing the given attribute value; otherwise an empty string.</returns>
        public static IHtmlString RouteIf(this HtmlHelper helper, string value, string attribute)
        {

            var currentController =
                (helper.ViewContext.RequestContext.RouteData.Values["controller"] ?? string.Empty).ToString().UnDash();
            var currentAction =
                (helper.ViewContext.RequestContext.RouteData.Values["action"] ?? string.Empty).ToString().UnDash();

            var hasController = value.Contains(currentController);

            var hasAction = value.Contains(currentAction);

            return hasController || string.Join(",", value.ToString()).Contains(currentController) ? new HtmlString(attribute) : new HtmlString(string.Empty);
        }

        public static IHtmlString RouteIfRemoveAttribute(this HtmlHelper helper, string value, string attribute)
        {

            var currentController =
                (helper.ViewContext.RequestContext.RouteData.Values["controller"] ?? string.Empty).ToString().UnDash();
            var currentAction =
                (helper.ViewContext.RequestContext.RouteData.Values["action"] ?? string.Empty).ToString().UnDash();

            var hasController = value.Contains(currentController);

            var hasAction = value.Contains(currentAction);

            return hasController || string.Join(",", value.ToString()).Contains(currentController) ? new HtmlString(string.Empty) : new HtmlString(attribute);
        }

        /// <summary>
        /// used to activate the left nav child menu
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="controller"></param>
        /// <param name="action"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public static IHtmlString RouteActionIf(this HtmlHelper helper, string controller, string action, string attribute, string detailAction = "")
        {
            var currentController =
                (helper.ViewContext.RequestContext.RouteData.Values["controller"] ?? string.Empty).ToString().UnDash();
            var currentAction =
                (helper.ViewContext.RequestContext.RouteData.Values["action"] ?? string.Empty).ToString().UnDash();
            var hasController = controller.Equals(currentController, StringComparison.InvariantCultureIgnoreCase);
            var hasAction = (action.Equals(currentAction, StringComparison.InvariantCultureIgnoreCase));
            #region Code for handle url isreadMode and isneworderplaced 

            //convert actions into array
            string[] detailActionArray = detailAction.Split(',');
            //get current path of url
            string currentPath = new Uri(HttpContext.Current.Request.Url.AbsoluteUri).OriginalString;
            string queryStringData = currentPath.Split('?').Length > 1 ? currentPath.Split('?')[0].ToString() : currentPath;
            string[] currentPathSubString = queryStringData.Split('/');
            if (detailAction!="" && detailActionArray.Length > 0)
            {
                foreach (var item in detailActionArray)
                {
                    bool isTrue = currentPathSubString.Any(x => x == item);
                    if (isTrue)
                    {
                        hasAction = true;
                        break;
                    }
                }
            }
            #endregion 
            return hasAction && hasController ? new HtmlString(attribute) : new HtmlString(string.Empty);
        }

        /// <summary>
        /// used to activate the left nav child menu while page is on subpages
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="controller"></param>
        /// <param name="action"></param>
        /// <param name="innerPage"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public static IHtmlString RouteInnerPageIf(this HtmlHelper helper, string controller, string action, string innerPage, string attribute, string detailAction = "")
        {

            var currentController =
                (helper.ViewContext.RequestContext.RouteData.Values["controller"] ?? string.Empty).ToString().UnDash();
            var currentAction =
                (helper.ViewContext.RequestContext.RouteData.Values["action"] ?? string.Empty).ToString().UnDash();
            var hasController = controller.Equals(currentController, StringComparison.InvariantCultureIgnoreCase);
            var hasAction = (action.Equals(currentAction, StringComparison.InvariantCultureIgnoreCase));
            var hasInnerPage = (innerPage.Equals(currentAction, StringComparison.InvariantCultureIgnoreCase));
            #region Code for handle url isreadMode and isneworderplaced 

            //convert actions into array
            string[] detailActionArray = detailAction.Split(',');
            //get current path of url
            string currentPath = new Uri(HttpContext.Current.Request.Url.AbsoluteUri).OriginalString;
            string queryStringData = currentPath.Split('?').Length > 1 ? currentPath.Split('?')[0].ToString() : currentPath;
            string[] currentPathSubString = queryStringData.Split('/');
            if (detailActionArray.Length > 1)
            {
                foreach (var item in detailActionArray)
                {
                    bool isTrue = currentPathSubString.Any(x => x == item);
                    if (isTrue)
                    {
                        hasAction = true;
                        break;
                    }
                }
            }

            bool actionFound = (hasInnerPage || string.Join(",", innerPage.ToString()).Contains(currentAction) || hasAction) ? true : false;
            bool controllerFound = (hasController || string.Join(",", controller.ToString()).Contains(currentController)) ? true : false;

            #endregion
            return actionFound && controllerFound ? new HtmlString(attribute) : new HtmlString(string.Empty);
        }



        /// <summary>
        ///     Removes dashes ("-") from the given object value represented as a string and returns an empty string ("")
        ///     when the instance type could not be represented as a string.
        ///     <para>
        ///         Note: This will return the type name of given isntance if the runtime type of the given isntance is not a
        ///         string!
        ///     </para>
        /// </summary>
        /// <param name="value">The object instance to undash when represented as its string value.</param>
        /// <returns></returns>
        public static string UnDash(this object value)
        {
            return ((value as string) ?? string.Empty).UnDash();
        }

        /// <summary>
        ///     Removes dashes ("-") from the given string value.
        /// </summary>
        /// <param name="value">The string value that optionally contains dashes.</param>
        /// <returns></returns>
        public static string UnDash(this string value)
        {
            return (value ?? string.Empty).Replace("-", string.Empty);
        }



    }
}