﻿using Event.Data.Datamodels.Context;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Library.Migrations
{
    public class Configuration : DbMigrationsConfiguration<EventContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        //protected override void Seed(EventContext eventContext)
        //{
        //    var dc = new EventContext();

        //    #region ADD SOME DEFAULT VALUE INTO PROGRAM-STYLES TABLE
        //    var programStyleDbs = new List<ProgramStyle>();

        //    if (!dc.ProgramStyles.Any(x => x.Name == "Points"))
        //    {
        //        //points
        //        var point = new ProgramStyle()
        //        {
        //            Name = "Points",
        //            IsActive = true
        //        };
        //        programStyleDbs.Add(point);
        //    }
        //    if (!dc.ProgramStyles.Any(x => x.Name == "Punch Card"))
        //    {
        //        //punchCard
        //        var punchCard = new ProgramStyle()
        //        {
        //            Name = "Punch Card",
        //            IsActive = true
        //        };
        //        programStyleDbs.Add(punchCard);
        //    }
        //    if (!dc.ProgramStyles.Any(x => x.Name == "Coupon"))
        //    {
        //        //coupon
        //        var coupon = new ProgramStyle()
        //        {
        //            Name = "Coupon",
        //            IsActive = true
        //        };
        //        programStyleDbs.Add(coupon);
        //    }
        //    if (!dc.ProgramStyles.Any(x => x.Name == "Voucher"))
        //    {
        //        //voucher
        //        var voucher = new ProgramStyle()
        //        {
        //            Name = "Voucher",
        //            IsActive = true
        //        };
        //        programStyleDbs.Add(voucher);
        //    }
        //    if (!dc.ProgramStyles.Any(x => x.Name == "Linkshare"))
        //    {
        //        //linkshare
        //        var linkshare = new ProgramStyle()
        //        {
        //            Name = "Linkshare",
        //            IsActive = true
        //        };
        //        programStyleDbs.Add(linkshare);
        //    }
        //    if (!dc.ProgramStyles.Any(x => x.Name == "Cardlink"))
        //    {
        //        //cardlink
        //        var cardlink = new ProgramStyle()
        //        {
        //            Name = "Cardlink",
        //            IsActive = true
        //        };
        //        programStyleDbs.Add(cardlink);
        //    }

        //    if (programStyleDbs != null && programStyleDbs.Count > 0)
        //    {
        //        dc.ProgramStyles.AddRange(programStyleDbs);
        //        base.Seed(dc);
        //        dc.SaveChanges();
        //    }
        //    #endregion



        //    #region ADD SOME DEFAULT VALUE INTO PROGRAM-STYLES TABLE
        //    var redemptionStyleDbs = new List<RedemptionStyle>();

        //    if (!dc.RedemptionStyles.Any(x => x.Name == "Voucher"))
        //    {
        //        //Voucher
        //        var voucher = new RedemptionStyle()
        //        {
        //            Name = "Voucher",
        //            IsActive = true
        //        };
        //        redemptionStyleDbs.Add(voucher);
        //    }
        //    if (!dc.RedemptionStyles.Any(x => x.Name == "Coupon"))
        //    {
        //        //Coupon
        //        var coupon = new RedemptionStyle()
        //        {
        //            Name = "Coupon",
        //            IsActive = true
        //        };
        //        redemptionStyleDbs.Add(coupon);
        //    }

        //    if (!dc.RedemptionStyles.Any(x => x.Name == "Prepaid"))
        //    {
        //        //Prepaid
        //        var prepaid = new RedemptionStyle()
        //        {
        //            Name = "Prepaid",
        //            IsActive = true
        //        };
        //        redemptionStyleDbs.Add(prepaid);
        //    }
        //    if (!dc.RedemptionStyles.Any(x => x.Name == "Catalogue"))
        //    {
        //        //coupon
        //        var coupon = new RedemptionStyle()
        //        {
        //            Name = "Catalogue",
        //            IsActive = true
        //        };
        //        redemptionStyleDbs.Add(coupon);
        //    }


        //    if (redemptionStyleDbs != null && redemptionStyleDbs.Count > 0)
        //    {
        //        dc.RedemptionStyles.AddRange(redemptionStyleDbs);
        //        base.Seed(dc);
        //        dc.SaveChanges();
        //    }
        //    #endregion


        //    #region ADD SOME DEFAULT VALUE INTO BarcodeStyle TABLE
        //    var barcopdeStyleDbs = new List<BarcodeStyle>();

        //    if (!dc.BarcodeStyles.Any(x => x.Name == "Style1"))
        //    {
        //        //Voucher
        //        var style1 = new BarcodeStyle()
        //        {
        //            Name = "Style1",
        //            IsActive = true
        //        };
        //        barcopdeStyleDbs.Add(style1);
        //    }
        //    if (!dc.RedemptionStyles.Any(x => x.Name == "Style2"))
        //    {
        //        //Coupon
        //        var style2 = new BarcodeStyle()
        //        {
        //            Name = "Style2",
        //            IsActive = true
        //        };
        //        barcopdeStyleDbs.Add(style2);
        //    }

        //    if (barcopdeStyleDbs != null && barcopdeStyleDbs.Count > 0)
        //    {
        //        dc.BarcodeStyles.AddRange(barcopdeStyleDbs);
        //        base.Seed(dc);
        //        dc.SaveChanges();
        //    }
        //    #endregion


        //    #region ADD SOME DEFAULT VALUE INTO Lookup TABLE
        //    var programPointTermDomain = new LookUpDomain();
        //    var programPointTermDomainValues = new List<LookUpDomainValue>();
        //    if (!dc.LookUpDomains.Any(x => x.Code == Core.Enums.LookUpDomain.ProgramPointTerm.ToString()))
        //    {
        //        programPointTermDomain = new LookUpDomain()
        //        {
        //            Code = Core.Enums.LookUpDomain.ProgramPointTerm.ToString(),
        //            Description = "Program Point Term"
        //        };
        //        dc.LookUpDomains.Add(programPointTermDomain);
        //        dc.SaveChanges();

        //        //
        //        programPointTermDomainValues.Add(
        //             new LookUpDomainValue()
        //             {
        //                 DomainId = programPointTermDomain.Id,
        //                 DomainValue = "Do not award fractional points",
        //                 DomainText = "Do not award fractional points"
        //             }
        //            );


        //        programPointTermDomainValues.Add(
        //           new LookUpDomainValue()
        //           {
        //               DomainId = programPointTermDomain.Id,
        //               DomainValue = "Round up to the next whole point value",
        //               DomainText = "Round up to the next whole point value"
        //           }
        //          );

        //        dc.LookUpDomainValues.AddRange(programPointTermDomainValues);
        //        dc.SaveChanges();

        //    }



        //    #endregion



        //}
    }
}
