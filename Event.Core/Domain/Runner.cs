﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Event.Core.Domain
{
    //•	Runner’s name, address, and category (Professional or Amature). 
    //•	The hosting city whose event the individual will attend.
    //•	A participant’s selected route: 5, 10 or 20 miles.
    //•	A participant’s selected charity, and the amount they have raised in sponsorship.

    [Table("Runners")]
    public class Runner : BaseEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }

        public string HostingCity { get; set; }

        public int Route { get; set; }

        //Name of the charity 
        public string Charity { get; set; }

        public decimal CharityFund { get; set; }

        // Enum RunnerCategory use this enum for creating the drop down 
        public int Category { get; set; }

        public int IsAddedByAdmin { get; set; }

    }
}
