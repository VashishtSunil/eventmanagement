﻿using Event.Core.Enums;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace Event.Core.Models
{
    //•	Runner’s name, address, and category (Professional or Amature). 
    //•	The hosting city whose event the individual will attend.
    //•	A participant’s selected route: 5, 10 or 20 miles.
    //•	A participant’s selected charity, and the amount they have raised in sponsorship.

    public class RunnerModel : BaseEntity
    {
        [Required(ErrorMessage = "Please enter name.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter Address.")]
        public string Address { get; set; }

         [Required(ErrorMessage = "Please enter Hosting City.")]
        public string HostingCity { get; set; }

        [Required(ErrorMessage = "Please select a route.")]
        [Range(1, int.MaxValue, ErrorMessage = "Please select a route.")]
        public EventRoute Route { get; set; }

        [Required(ErrorMessage = "Please enter Charity name.")]
        //Name of the charity 
        public string Charity { get; set; }

        [Required(ErrorMessage = "Please enter Charity fund.")]
        public decimal CharityFund { get; set; }


        [Required(ErrorMessage = "Please select runner category.")]
        [Range(1, int.MaxValue, ErrorMessage = "Please select runner category.")]
        // Enum RunnerCategory use this enum for creating the drop down 
        public RunnerCategory Category { get; set; }

        public int IsAddedByAdmin { get; set; }

        //List of entities

        public SelectList LstHostingCities { get; set; }

        public SelectList LstRoutes { get; set; }

    }
}
