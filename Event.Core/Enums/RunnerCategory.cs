﻿using System.ComponentModel;

namespace Event.Core.Enums
{
    public enum RunnerCategory
    {

        [Description("Professional")]
        Professional = 1,
        [Description("Amature")]
        Amature = 2
    }
}
