﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Core.Enums
{
    public enum EventRoute
    {
        [Display(Name = "5 Miles")]
        [Description("5 Miles")]
        RouteOne = 1,

        [Display(Name = "10 Miles")]
        [Description("10 Miles")]
        RouteTwo = 2,

        [Display(Name = "20 Miles")]
        [Description("20 Miles")]
        RouteThree = 3
    }
}
