﻿using CouponFactory.Core.Models;
using Event.Core.Enums;
using Event.Core.Models;
using Event.Data.Datamodels.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Event.Services.Classes.Runner
{
    public class RunnerManager
    {
        /// <summary>
        /// Used to add Add/Edit Runner
        /// </summary>
        /// <param name="runnerModel"></param>
        /// <returns></returns>
        public static bool UpdateRunner(RunnerModel runnerModel)
        {
            var dc = new EventContext();
            var runner = dc.Runners.FirstOrDefault(x => x.Id == runnerModel.Id);
            try
            {
                // for add new record
                if (runner == null)
                {
                    runner = new Core.Domain.Runner
                    {
                        Name = runnerModel.Name,
                        Address = runnerModel.Address,
                        HostingCity = runnerModel.HostingCity,
                        Route = (int)runnerModel.Route,
                        Charity = runnerModel.Charity,
                        CharityFund = runnerModel.CharityFund,
                        Category = (int)runnerModel.Category,
                        IsAddedByAdmin = runnerModel.IsAddedByAdmin,
                        CreatedBy = runnerModel.CreatedBy
                    };
                    dc.Runners.Add(runner);
                    dc.SaveChanges();
                }
                // for update 
                else
                {
                    runner.Name = runnerModel.Name;
                    runner.Address = runnerModel.Address;
                    runner.HostingCity = runnerModel.HostingCity;
                    runner.Route = (int)runnerModel.Route;
                    runner.Charity = runnerModel.Charity;
                    runner.CharityFund = runnerModel.CharityFund;
                    runner.Category = (int)runnerModel.Category;
                    runner.ModifiedBy = runnerModel.CreatedBy;
                    runner.IsAddedByAdmin = runnerModel.IsAddedByAdmin;
                    runner.ModifiedOn = DateTime.UtcNow;
                    dc.SaveChanges();
                }
                runnerModel.Id = runner.Id;
                return true;
            }
            catch (Exception ex)
            {
                // Log.Error(ex, "BrandManager=>>UpdateBrand");
                return false;
            }
        }

        /// <summary>
        /// Get all Data of Runner to bind with data-table 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static async Task<Tuple<List<RunnerModel>, string, int>> GetRunnersDataTable(jQueryDataTableParamModel param)
        {
            var dc = new EventContext();
            var sortColumnIndex = param.iSortCol_0;
            var qry = dc.Runners.Where(x => x.Id > 0);
            //  .Where(x => x.IsDeleted == false);
            //Get total count
            var totalRecords = await qry.CountAsync();


            #region Searching 
            if (!string.IsNullOrEmpty(param.sSearch))
            {
                var toSearch = param.sSearch.ToLower();
                qry = qry.Where(c => c.Name.ToLower().Contains(toSearch)
                ||
                c.HostingCity.ToLower().Contains(toSearch)
                 ||
                c.Address.ToLower().Contains(toSearch)
                 ||
                c.Charity.ToLower().Contains(toSearch)
                );
            }
            #endregion

            #region  Sorting 
            switch (sortColumnIndex)
            {
                //Sort by Name
                case 0:
                    switch (param.sSortDir_0)
                    {
                        case "desc":
                            qry = qry.OrderByDescending(a => a.Name);
                            break;
                        case "asc":
                            qry = qry.OrderBy(a => a.Name);
                            break;
                        default:
                            qry = qry.OrderBy(a => a.Name);
                            break;
                    }

                    break;
                //Sort by User Name
                case 1:
                    switch (param.sSortDir_0)
                    {
                        case "desc":
                            qry = qry.OrderByDescending(a => a.Address);
                            break;
                        case "asc":
                            qry = qry.OrderBy(a => a.Address);
                            break;
                        default:
                            qry = qry.OrderBy(a => a.Address);
                            break;
                    }
                    break;

                default:
                    qry = qry.OrderBy(a => a.Name);
                    break;
            }
            #endregion

            #region  Paging 
            if (param.iDisplayLength != -1)
                qry = qry.Skip(param.iDisplayStart).Take(param.iDisplayLength);
            #endregion


            return new Tuple<List<RunnerModel>, string, int>(
                await
                    (from p in qry
                     select new RunnerModel
                     {
                         Id = p.Id,
                         Name = p.Name,
                         Address = p.Address,
                         HostingCity = p.HostingCity,
                         Route = (EventRoute)p.Route,
                         Charity = p.Charity,
                         CharityFund = p.CharityFund,
                         Category = (RunnerCategory)p.Category,
                     }).ToListAsync(),
                param.sEcho,
                totalRecords);
        }

        /// <summary>
        /// Get Runner By Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static RunnerModel GetRunnerById(long Id)
        {
            var dc = new EventContext();
            return (from p in dc.Runners
                    where p.IsDeleted == false && p.Id == Id
                    //Binding Data with Model
                    select new RunnerModel
                    {
                        Name = p.Name,
                        Address = p.Address,
                        HostingCity = p.HostingCity,
                        Route = (EventRoute)p.Route,
                        Charity = p.Charity,
                        CharityFund = p.CharityFund,
                        Category = (RunnerCategory)p.Category,

                        //Base Entities
                        Id = p.Id,
                        CreatedOn = p.CreatedOn,
                        CreatedBy = p.CreatedBy,
                        ModifiedOn = p.ModifiedOn,
                        ModifiedBy = p.ModifiedBy

                    }).FirstOrDefault();
        }


        public static bool DeleteRunner(long Id)
        {
            var dc = new EventContext();
            var runner = dc.Runners.FirstOrDefault(x => x.Id == Id && x.IsDeleted == false);
            if (runner == null)
                return true;

            runner.IsDeleted = true;
            return dc.SaveChanges() > 0;
        }
    }
}
